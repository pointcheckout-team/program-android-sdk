# PointCheckout Program Android SDK #
## Getting started ##

These instructions will help you to use the SDK within your Android application.

### Setup ###

#### Step 1 ####
Copy `hermes-release.aar` from [here](https://bitbucket.org/pointcheckout-team/program-android-sdk/src/master/lib/libs/hermes-release.aar)  to `YourProject/app/libs`.

Now your working directory should look like this:

```
.
+-- YourProject
|   +-- app
|  |   +-- libs
|  |  |  +-- hermes-release.aar
```

#### Step 2 ####
#### Gradle =>6.8 ####
Add the following to `YourProject/settings.gradle`

```gradle
dependencyResolutionManagement {
    ...
    repositories {
       ...
        flatDir {
            dirs 'libs'
        }
        maven { url 'https://www.jitpack.io' }
    }
}
```
#### Gradle <6.8 ####
Add the following to `YourProject/build.gradle`

```gradle
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
        flatDir {
            dirs 'libs'
        }
    }
}
```


#### Step 3 ####
Add the following dependencies to `YourProject/app/build.gradle`

```gradle
    implementation 'org.bitbucket.pointcheckout-team:program-android-sdk:{tagName}'
    implementation(name:'hermes-release', ext:'aar')
```
> NOTE: replace {tagName} with a tag from the tags sections, the latest version at the time writing this documentaion was v1.0.10

#### Step 4 ####
Add the following to `YourProject/app/build.gradle`
```gradle
android {
    defaultConfig {
       ...
        multiDexEnabled true
    }
    packagingOptions {
        exclude '**/libhermes-inspector.so'
        exclude '**/libhermes-executor-debug.so'
        exclude '**/libjscexecutor.so'
    }
}
```

#### Step 5 ####
Add the following permissions to `AndroidManifest.xml`

```xml
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.VIBRATE"/>
    
    <application 
        .....
    >
        ....
        <activity android:name="com.pc.android.program.sdk.PointCheckoutActivity" />
    </application>
```

### Are you using Proguard? ###
If you use Proguard you will need to add these lines to `YourProject/app/proguard-rules.pro`

```
-keep public class com.pc.android.program.sdk.PointCheckoutClient { *; }
-keep public class com.pc.android.program.sdk.PointCheckoutActivity { *; }
-keep public class com.pc.android.program.sdk.PointCheckoutEnvironment { *; }
-keep public class com.pc.android.program.sdk.PointCheckoutScreen { *; }
-keep public class com.pc.android.program.sdk.PointCheckoutTheme { *; }
-keep public class com.pc.android.program.sdk.PointCheckoutEvent { *; }
-keep public class com.swmansion.gesturehandler.** {*;}
-keep public class com.babisoft.ReactNativeLocalizationr.** {*;}
-keep public class com.reactcommunity.rnlocalize.** {*;}
-keep public class com.th3rdwave.safeareacontext.** {*;}
-keep public class com.reactnativecommunity.slider.** {*;}
-keep public class com.azendoo.reactnativesnackbar.** {*;}
-keep public class com.horcrux.svg.** {*;}
-keep public class com.oblador.vectoricons.** {*;}
-keep public class com.reactnativecommunity.webview.** {*;}
-keep class com.facebook.hermes.unicode.** { *; }
```

At this point, the SDK is integerated and ready to be used.

## Using the SDK ##
### Step 1: Initialization ###

The SDK needs to be initialized before showing a screen, its best to initialize the SDK after app startup. However, you can initialize the SDK right before showing a screen but the performance will be reduced.

To initialize the SDK you need a `config` object and an application context. Bellow is a full example on how to initialize the SDK:

```java
        // setup your theme here
        PointCheckoutTheme theme = new PointCheckoutTheme();
        
        // create a config object
        PointCheckoutConfig config= new PointCheckoutConfig.Builder()//
            .environment(PointCheckoutEnvironment.TEST) // set the env [TEST, PRODUCTION]
            .language(PointCheckoutLanguage.ENGLISH) // set the language [ARABIC, ENGLISH]
            .theme(theme) // set the theme
            .handleExceptions(true) // if true the SDK will show a message on errors
            .build();

        // initialize the SDK
        PointCheckoutClient.initialize(getApplication(), config);

        // event listener to be used for analytics
        PointCheckoutClient.onAnalyticsLog(new PointCheckoutAnalyticsEventListener() {
            @Override
            public void onEvent(String event, Map<String, Object> payload) {
                System.out.println(event);
            }
        });

        // this will be called whenever an exception is thrown
        // intended to be used for reporting exceptions
        PointCheckoutClient.onException(new PointCheckoutEventListener() {
            @Override
            public void onEvent(@Nullable String value) {
                System.out.println(value);
            }
        });
```

> NOTE: Initializing the SDK more than once has no effect

### Step 2: Start PointCheckoutActivity ###

To start PointCheckoutActivity, an `authToken` is needed, this token can be obtained from PointCheckout API.

```swift
        Intent intent = new Intent(MainActivity.this, PointCheckoutActivity.class);
        intent.putExtra(PointCheckoutConfig.AUTH_TOKEN, "authToken");
        
        // to open the main screen
        intent.putExtra(PointCheckoutConfig.OPEN_SCREEN, PointCheckoutScreen.MAIN_SCREEN.name());
        
        // to open merchant list screen
        // intent.putExtra(PointCheckoutConfig.MERCHANT_SCREEN, PointCheckoutScreen.MAIN_SCREEN.name());
        
        // to open a specific merchant screen
        // intent.putExtra(PointCheckoutConfig.OPEN_SCREEN, PointCheckoutScreen.MERCHANT_DETAILS_SCREEN.name());
        // intent.putExtra(PointCheckoutConfig.SCREEN_PARAM_ID, "merchantId");
        
        startActivity(intent);
```

## Deep Linking ##
If you dont already have Deep Linking enabled, follow [this guid](https://developer.android.com/training/app-links/deep-linking).

### Step 1 ###
Add the following intent filter to your activity:
```xml
<intent-filter>
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" />
    <data 
    android:scheme="https" 
    android:host="{URL}" 
    android:pathPrefix="/pc-checkout" /> 
    <data 
    android:scheme="https" 
    android:host="{URL}" 
    android:pathPrefix="/program-checkout/{PROGRAM_CODE}" /> 
</intent-filter>
```

PointCheckout will provide you with the `{PROGRAM_CODE}`.
Replace `{URL}` with PointCheckout URL:

| Environment        | URL                                                                                                                                  |
|--------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| Test | pay.test.pointcheckout.com |
| Production    | pay.pointcheckout.com|

### Step 2 ###
Send all PointCheckout URLs to the SDK:
```swift
 PointCheckoutClient.handleDeeplinkingUrl("The URL");
```


## PointCheckoutTheme ##
This class is used to customize the UI of the SDK. The table below shows the basic properties:

| Property                | Type                                  | Default      | Description              |
|-------------------------|---------------------------------------|--------------|--------------------------|
| backgroundImageDrawable | Integer (Drawable resource)           | null         | Background image         |
| headerImageDrawable     | Integer (Drawable resource)           | null         | Main screen header image |
| headerVisible           | boolean                               | true         | Hide or show the header  |
| statusBar               | ENUM("light-content", "dark-content") | dark-content | Status bar theme         |
| themeColor              | String                                | #ee8801      | Main theme color         |
| backgroundColor         | String                                | #fafafa      | Background color         |
| foregroundColor         | String                                | #4A4A4A      | Foreground color         |

## License ##
PointCheckout.com. All Rights Reserved.