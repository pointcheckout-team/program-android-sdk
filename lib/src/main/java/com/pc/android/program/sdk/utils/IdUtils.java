package com.pc.android.program.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

public class IdUtils {

    private static final String POINTCEHCKOUT_PREFERENCES = "pointcheckout.program.sdk.preferences";
    private static final String KEY_DEVICE_ID = "POINTCHECKOUT_PROGRAMSDK_DEVICE_ID";

    public static String getDeviceId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(POINTCEHCKOUT_PREFERENCES, Context.MODE_PRIVATE);
        String deviceId = sharedPref.getString(KEY_DEVICE_ID, null);
        if (deviceId == null) {
            deviceId = UUID.randomUUID().toString();
            setDeviceId(context, deviceId);
        }
        return deviceId;
    }

    public static void setDeviceId(Context context, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences(POINTCEHCKOUT_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_DEVICE_ID, value);
        editor.commit();
    }
}
