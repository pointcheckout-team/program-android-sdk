package com.pc.android.program.sdk;

import androidx.annotation.Nullable;

import java.util.Map;

public interface PointCheckoutAnalyticsEventListener {
    void onEvent(String event, Map<String, Object> payload);
}
