package com.pc.android.program.sdk.internal.reactnative;

import java.util.Map;

public class RNAnalyticsEvent {
    private String event;
    private Map<String, Object> payload;

    public void setEvent(String event) {
        this.event = event;
    }

    public void setPayload(Map<String, Object> payload) {
        this.payload = payload;
    }

    public String getEvent() {
        return event;
    }

    public Map<String, Object> getPayload() {
        return payload;
    }

}
