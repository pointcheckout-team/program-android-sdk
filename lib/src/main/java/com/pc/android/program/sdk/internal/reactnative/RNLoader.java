package com.pc.android.program.sdk.internal.reactnative;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.azendoo.reactnativesnackbar.SnackbarPackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactInstanceManagerBuilder;
import com.facebook.react.ReactPackage;
import com.facebook.react.ReactRootView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.horcrux.svg.SvgPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.pc.android.program.sdk.PointCheckoutConfig;
import com.pc.android.program.sdk.PointCheckoutEnvironment;
import com.pc.android.program.sdk.PointCheckoutScreen;
import com.pc.android.program.sdk.PointCheckoutEvent;
import com.pc.android.program.sdk.internal.event.InternalEvent;
import com.pc.android.program.sdk.utils.IdUtils;
import com.reactcommunity.rnlocalize.RNLocalizePackage;
import com.reactnativecommunity.slider.ReactSliderPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.th3rdwave.safeareacontext.SafeAreaContextPackage;

import org.reactnative.camera.RNCameraPackage;

import java.util.Arrays;
import java.util.List;

import iyegoroff.RNColorMatrixImageFilters.ColorMatrixImageFiltersPackage;

public class RNLoader {

    private static final String APP_NAME = "programsdk";
    private static final String EVENT_NAME = "onEvent";
    private static final String EVENT_EVENT = "event";
    private static final String EVENT_DATA = "data";
    private static final String ENV = "ENV";
    private static final String THEME = "THEME";
    private static final String LANGUAGE = "LANGUAGE";
    private static final String HANDLE_EXCEPTIONS = "HANDLE_EXCEPTIONS";
    private static final String DEEPLINK = "DEEPLINK";
    private static final String DEVICE_ID = "DEVICE_ID";
    private static final String SCREEN_PARAMS = "SCREEN_PARAMS";

    private ReactInstanceManager mReactInstanceManager;
    private ReactRootView mReactRootView;
    private Application application;
    private PointCheckoutConfig config;


    public RNLoader(Application application, PointCheckoutConfig config) {
        this.application = application;
        this.config = config;

        SoLoader.init(getContext(), false);

        ReactInstanceManagerBuilder builder = ReactInstanceManager.builder()
                .setApplication(application)
                .setJSMainModulePath("index")
                .addPackages(getPackages())
                .setInitialLifecycleState(LifecycleState.BEFORE_CREATE);

        if (this.config.environment == PointCheckoutEnvironment.DEBUG) {
            builder.setUseDeveloperSupport(true);
        } else {
            builder.setBundleAssetName("index.android.bundle");
        }

        mReactInstanceManager = builder.build();
        mReactRootView = new ReactRootView(getContext());

        mReactRootView.startReactApplication(mReactInstanceManager, APP_NAME, getInitialProps());
    }

    public void onReactEvent(PointCheckoutEvent event) {
        onReactEvent(event, null);
    }

    public void onReactEvent(PointCheckoutEvent event, @Nullable String data) {
        onReactEvent(event.name(), data);
    }

    public void onReactEvent(InternalEvent event, String data) {
        onReactEvent(event.name(), data);
    }

    public void onReactEvent(String event, String data) {
        WritableMap params = Arguments.createMap();
        params.putString(EVENT_EVENT, event);
        params.putString(EVENT_DATA, data);
        this.sendEvent(this.mReactInstanceManager.getCurrentReactContext(), EVENT_NAME, params);
    }

    public ReactRootView getReactRootView() {

        try {
            mReactRootView.unmountReactApplication();
        }catch(Exception e){
            // already unmounted
        }

        mReactRootView = new ReactRootView(getContext());
        mReactRootView.startReactApplication(mReactInstanceManager, APP_NAME, getInitialProps());
        return mReactRootView;
    }

    public ReactInstanceManager getReactInstanceManager() {
        return mReactInstanceManager;
    }

    protected List<ReactPackage> getPackages() {
        return Arrays.asList(
                new MainReactPackage(),
                new RNGestureHandlerPackage(),
                new SafeAreaContextPackage(),
                new RNLocalizePackage(),
                new ReactNativeLocalizationPackage(),
                new SnackbarPackage(),
                new VectorIconsPackage(),
                new ColorMatrixImageFiltersPackage(),
                new RNCWebViewPackage(),
                new ReactSliderPackage(),
                new SvgPackage(),
                new RNCameraPackage(),
                new RNEventHandlerPackage()
        );
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    private Context getContext() {
        return application.getApplicationContext();
    }

    private Bundle getInitialProps() {
        final Bundle initialProperties = new Bundle();
        if(this.config.screen!=null)
            initialProperties.putString(PointCheckoutConfig.OPEN_SCREEN, this.config.screen.getRoute());
        initialProperties.putString(SCREEN_PARAMS, this.config.paramsJson);
        initialProperties.putString(ENV, this.config.environment.name());
        initialProperties.putString(PointCheckoutConfig.AUTH_TOKEN, this.config.authToken);
        initialProperties.putString(THEME, this.config.theme.serialize(this.application));
        initialProperties.putString(LANGUAGE, this.config.language.getIso2());
        initialProperties.putString(HANDLE_EXCEPTIONS, Boolean.toString(this.config.handleExceptions));
        initialProperties.putString(DEEPLINK, this.config.deeplinkingUrl);
        initialProperties.putString(DEVICE_ID, IdUtils.getDeviceId(getContext()));
        return initialProperties;
    }

    public void updateConfig(Intent intent){
        this.config.authToken = intent.getStringExtra(PointCheckoutConfig.AUTH_TOKEN);
        if(intent.getStringExtra(PointCheckoutConfig.OPEN_SCREEN)!=null)
            this.config.screen = PointCheckoutScreen.valueOf(intent.getStringExtra(PointCheckoutConfig.OPEN_SCREEN));
        if(intent.getStringExtra(PointCheckoutConfig.SCREEN_PARAM_ID) !=null)
            this.config.paramsJson = String.format("{\"id\":\"%s\"}",intent.getStringExtra(PointCheckoutConfig.SCREEN_PARAM_ID));
    }

    public PointCheckoutConfig getConfig(){
        return config;
    }

}
