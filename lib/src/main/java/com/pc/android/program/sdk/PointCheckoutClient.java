package com.pc.android.program.sdk;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.UiThreadUtil;
import com.pc.android.program.sdk.internal.event.EventManager;
import com.pc.android.program.sdk.internal.event.InternalEvent;
import com.pc.android.program.sdk.internal.reactnative.RNAnalyticsEvent;
import com.pc.android.program.sdk.internal.reactnative.RNLoader;
import com.pc.android.program.sdk.utils.IdUtils;
import com.pc.android.program.sdk.utils.JsonUtils;

public class PointCheckoutClient {

    private static RNLoader rnLoader;
    private static boolean initialized = false;
    private static boolean waitingIntialization = false;
    private static boolean onExceptionListener = false;
    private static boolean onAnalyticsLogListener = false;
    private static String deeplink = null;


    public static void initialize(@NonNull final Application application, final PointCheckoutConfig config) {
        if (initialized) {
            rnLoader.getConfig().language = config.language;
            return;
        }

        UiThreadUtil.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rnLoader = new RNLoader(application, config);
                EventManager.instance.addEventListener(PointCheckoutEvent.INITIALIZED, new PointCheckoutEventListener() {
                    @Override
                    public void onEvent(@Nullable String value) {
                        initialized = true;
                        if (waitingIntialization) {
                            sdkOpen();
                            waitingIntialization = false;
                        }
                        PointCheckoutClient.handleDeeplinkingUrl();
                    }
                });
            }
        });

    }

    public static void onBackPressed(PointCheckoutEventListener listener) {
        EventManager.instance.addEventListener(PointCheckoutEvent.EXIT_REQUEST, listener);
    }

    public static void goBack() {

        rnLoader.onReactEvent(PointCheckoutEvent.GO_BACK);
    }

    public static void onException(PointCheckoutEventListener listener) {
        if (onExceptionListener)
            return;
        onExceptionListener = true;
        EventManager.instance.addEventListener(PointCheckoutEvent.EXCEPTION, listener);
    }

    public static void onAnalyticsLog(final PointCheckoutAnalyticsEventListener listener) {
        if (onAnalyticsLogListener)
            return;
        onAnalyticsLogListener = true;
        EventManager.instance.addEventListener(PointCheckoutEvent.LOG_ANALYTICS, new PointCheckoutEventListener() {
            @Override
            public void onEvent(@Nullable String value) {
                RNAnalyticsEvent event = JsonUtils.decode(value, RNAnalyticsEvent.class);
                listener.onEvent(event.getEvent(), event.getPayload());
            }
        });
    }

    public static void setDeeplinkingUrl(String url) {
        deeplink = url;
    }

    public static String getDeeplinkingUrl() {
        return deeplink;
    }

    public static void handleDeeplinkingUrl() {
        if (deeplink != null) {
            rnLoader.onReactEvent(InternalEvent.DEEPLINKING, deeplink);
            deeplink = null;
        }
    }

    public static void sdkOpen() {
        rnLoader.onReactEvent(InternalEvent.SDK_OPEN, null);
    }

    public static String getDeviceId(Context context) {
        return IdUtils.getDeviceId(context);
    }

    public static ReactRootView getReactRootView() {
        return rnLoader.getReactRootView();
    }

    public static ReactInstanceManager getReactInstanceManager() {
        if (rnLoader == null)
            return null;
        return rnLoader.getReactInstanceManager();
    }

    public static void updateConfig(Intent intent) {
        rnLoader.updateConfig(intent);
    }

    public static PointCheckoutConfig getConfig() {
        return rnLoader.getConfig();
    }

    public static boolean isInitialized() {
        return initialized;
    }

    public static void setWaitingIntialization() {
        waitingIntialization = true;
    }

}
