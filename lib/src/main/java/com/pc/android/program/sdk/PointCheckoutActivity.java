package com.pc.android.program.sdk;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;

import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.modules.core.PermissionAwareActivity;
import com.facebook.react.modules.core.PermissionListener;
import com.facebook.react.modules.i18nmanager.I18nUtil;

public class PointCheckoutActivity extends FragmentActivity implements DefaultHardwareBackBtnHandler, PermissionAwareActivity {



    @Nullable
    private PermissionListener mPermissionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PointCheckoutClient.updateConfig(getIntent());

        boolean rtl = PointCheckoutClient.getConfig().language == PointCheckoutLanguage.ARABIC;
        I18nUtil sharedI18nUtilInstance = I18nUtil.getInstance();
        sharedI18nUtilInstance.forceRTL(this,rtl);
        sharedI18nUtilInstance.allowRTL(this, rtl);

        setContentView(PointCheckoutClient.getReactRootView());
        PointCheckoutClient.onBackPressed(new PointCheckoutEventListener() {
            @Override
            public void onEvent(@Nullable String value) {
                PointCheckoutActivity.this.finish();
            }
        });
        if(PointCheckoutClient.isInitialized())
            PointCheckoutClient.sdkOpen();
        else
            PointCheckoutClient.setWaitingIntialization();

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void requestPermissions(String[] permissions, int requestCode, PermissionListener listener) {
        mPermissionListener = listener;
        this.requestPermissions(permissions, requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (mPermissionListener != null && mPermissionListener.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
            mPermissionListener = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (PointCheckoutClient.getReactInstanceManager() != null) {
            PointCheckoutClient.getReactInstanceManager().onHostPause(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (PointCheckoutClient.getReactInstanceManager() != null) {
            PointCheckoutClient.getReactInstanceManager().onHostResume(this, this);
        }

        if(PointCheckoutClient.isInitialized())
            PointCheckoutClient.handleDeeplinkingUrl();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (PointCheckoutClient.getReactInstanceManager() != null) {
            PointCheckoutClient.getReactInstanceManager().onHostDestroy(this);
        }
        if (PointCheckoutClient.getReactRootView() != null) {
            PointCheckoutClient.getReactRootView().unmountReactApplication();
        }
    }

    @Override
    public void onBackPressed() {
        if (PointCheckoutClient.getReactInstanceManager() != null) {
            PointCheckoutClient.getReactInstanceManager().onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}
