package com.pc.android.program.sdk;

public enum PointCheckoutEvent {
    EXIT_REQUEST,
    EXCEPTION,
    LOG_ANALYTICS,
    INITIALIZED,
    GO_BACK,
}
