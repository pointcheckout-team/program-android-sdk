package com.pc.android.program.sdk;

public class PointCheckoutConfig {
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String OPEN_SCREEN = "OPEN_SCREEN";
    public static final String SCREEN_PARAM_ID = "SCREEN_PARAM_ID";

    public PointCheckoutEnvironment environment;
    public PointCheckoutScreen screen;
    public String paramsJson;
    public String authToken;
    public PointCheckoutTheme theme;
    public PointCheckoutLanguage language;
    public boolean handleExceptions;
    public String deeplinkingUrl;

    public static Builder Builder(){
        return new Builder();
    }

    public static class Builder {
        private PointCheckoutConfig instance;

        public Builder(){
            instance = new PointCheckoutConfig();
        }

        public Builder environment(PointCheckoutEnvironment environment){
            instance.environment = environment;
            return this;
        }

        public Builder screen(PointCheckoutScreen screen){
            instance.screen = screen;
            return this;
        }

        public Builder paramsJson(String paramsJson){
            instance.paramsJson = paramsJson;
            return this;
        }

        public Builder authToken(String authToken){
            instance.authToken = authToken;
            return this;
        }

        public Builder theme(PointCheckoutTheme theme){
            instance.theme = theme;
            return this;
        }

        public Builder language(PointCheckoutLanguage language){
            instance.language = language;
            return this;
        }

        public Builder handleExceptions(boolean handleExceptions){
            instance.handleExceptions = handleExceptions;
            return this;
        }

        public Builder deeplinkingUrl(String deeplinkingUrl){
            instance.deeplinkingUrl = deeplinkingUrl;
            return this;
        }

        public PointCheckoutConfig build(){
            return this.instance;
        }
    }
}
